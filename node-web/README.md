# AutoKubeOps Node Web Buildpack

## `registry.gitlab.com/autokubeops/buildpacks/node-web:0.1.0`

The AutoKubeOps Node Web Buildpack provides a set of collaborating buildpacks that
enable the building of a Node.js-based static application. These buildpacks include:
- [Node Engine CNB](https://github.com/paketo-buildpacks/node-engine)
- [NPM Compile CNB](../npm-ci/README.md)
- [Miniserv CNB](../miniserv)

The buildpack supports building/running simple Web applications which utilize [NPM](https://www.npmjs.com/) for managing their dependencies.
Yarn support is not yet available.

#### The Node.js buildpack is compatible with the following builder(s):
- [Paketo Full Builder](https://github.com/paketo-buildpacks/full-builder)
- [Paketo Base Builder](https://github.com/paketo-buildpacks/base-builder) (for apps which do not leverage common C libraries)

This buildpack also includes the following utility buildpacks:
- [Procfile CNB](https://github.com/paketo-buildpacks/procfile)
- [Environment Variables CNB](https://github.com/paketo-buildpacks/environment-variables)
- [Image Labels CNB](https://github.com/paketo-buildpacks/image-labels)
- [CA Certificates CNB](https://github.com/paketo-buildpacks/ca-certificates)
- [Node Run Script CNB](https://github.com/paketo-buildpacks/node-run-script)
- [Node Module Bill of Materials CNB](https://github.com/paketo-buildpacks/node-module-bom)
