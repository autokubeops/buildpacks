package main

import (
	"fmt"
	"github.com/kelseyhightower/envconfig"
	"github.com/paketo-buildpacks/packit/scribe"
	log "github.com/sirupsen/logrus"
	"gitlab.com/autokubeops/buildpacks/lifecycle/internal/phase"
	"os"
	"path/filepath"
	"strings"
)

type environment struct {
	ApplicationImage string `split_words:"true"`
	Registry         struct {
		Addr     string `envconfig:"CI_REGISTRY"`
		User     string `split_words:"true"`
		Password string `split_words:"true"`
		Image    string `split_words:"true"`
	}
	Project struct {
		Dir   string `split_words:"true" default:"/workspace/source"`
		Title string `split_words:"true"`
	}
	Commit struct {
		Sha       string `split_words:"true"`
		ShortSha  string `split_words:"true"`
		RefName   string `split_words:"true"`
		BeforeSha string `split_words:"true"`
		Branch    string `split_words:"true"`
		Tag       string `split_words:"true"`
	}
	Tags         string `envconfig:"CNB_TAGS"`
	JobStartedAt string `split_words:"true"`

	RunImage string `envconfig:"AUTO_CNB_RUN_IMAGE" default:"gcr.io/paketo-buildpacks/run:base-cnb"`
}

func main() {
	logger := scribe.NewLogger(os.Stdout)

	var e environment
	if err := envconfig.Process("ci", &e); err != nil {
		log.WithError(err).Fatal("failed to read environment")
		return
	}

	// prepare
	logger.Title("Preparing")
	logger.Process("Injecting authentication")

	logger.Detail("registry: %s@%s", os.Getenv("CI_REGISTRY_USER"), os.Getenv("CI_REGISTRY"))

	// write docker auth
	err := writeValue("/home/cnb/.docker/config.json", fmt.Sprintf(`{
	"auths": {
		"%s": {
			"username": "%s",
			"password": "%s"
		}
	}
}`, e.Registry.Addr, e.Registry.User, e.Registry.Password))
	if err != nil {
		return
	}

	logger.Process("Exporting environment")
	platformDir := os.Getenv("CNB_PLATFORM_DIR")
	if platformDir == "" {
		platformDir = "/platform"
	}

	_ = writeValue(filepath.Join(platformDir, "/env/BP_OCI_CREATED"), e.JobStartedAt)
	_ = writeValue(filepath.Join(platformDir, "/env/BP_OCI_REVISION"), e.Commit.Sha)
	_ = writeValue(filepath.Join(platformDir, "/env/BP_OCI_REF_NAME"), e.Commit.RefName)
	_ = writeValue(filepath.Join(platformDir, "/env/BP_OCI_TITLE"), e.Project.Title)

	var k, v string
	for _, i := range os.Environ() {
		bits := strings.Split(i, "=")
		k = bits[0]
		v = bits[1]
		if strings.HasPrefix(k, "BP_") {
			logger.Subdetail("%s=%s", k, v)
			_ = writeValue(filepath.Join(platformDir, fmt.Sprintf("/env/%s", k)), v)
		}
	}

	// check uid and run-image
	uid := os.Getuid()
	gid := os.Getgid()

	logger.Process("BuildPack configuration")
	logger.Detail("user: %d", uid)
	logger.Detail("group: %d", gid)
	logger.Subdetail("OpenShift compatible: %v", gid == 0)

	subPath := os.Getenv("SOURCE_SUBPATH")
	if subPath == "" {
		subPath = os.Getenv("PROJECT_PATH")
	}

	registryImage := e.Registry.Image
	if subPath != "" {
		registryImage = fmt.Sprintf("%s/%s", e.Registry.Image, subPath)
	}

	// collect images
	images := []string{
		fmt.Sprintf("-tag=%s:%s", registryImage, e.Commit.Sha),
		fmt.Sprintf("-tag=%s:%s", registryImage, e.Commit.ShortSha),
	}
	if e.ApplicationImage != "" {
		images = append(images, fmt.Sprintf("-tag=%s", e.ApplicationImage))
	}
	if e.Commit.Branch != "" {
		images = append(images, fmt.Sprintf("-tag=%s:%s", registryImage, strings.ReplaceAll(e.Commit.Branch, "/", "-")))
	}
	if e.Commit.Tag != "" {
		images = append(images, fmt.Sprintf("-tag=%s:%s", registryImage, e.Commit.Tag))
	}
	if e.Tags != "" {
		for _, t := range strings.Split(e.Tags, ",") {
			images = append(images, fmt.Sprintf("-tag=%s:%s", registryImage, t))
		}
	}
	logger.Process("Collecting tags")
	for _, t := range images {
		logger.Subdetail(t)
	}

	logger.Title("Creating")
	call(func() error {
		return phase.Create(e.Project.Dir, subPath, registryImage, e.Commit.BeforeSha, e.RunImage, images...)
	})
	logger.Title("Signing")
	// check if we should be signing
	if os.Getenv("CNB_SIGN_ENABLED") != "true" || os.Getenv("COSIGN_PRIVATE_KEY") == "" || os.Getenv("COSIGN_PASSWORD") == "" {
		logger.Process("Skipping (make sure CNB_SIGN_ENABLED, COSIGN_PRIVATE_KEY, COSIGN_PASSWORD are set)")
		return
	}
	logger.Process("Using signing key: %s", os.Getenv("COSIGN_PRIVATE_KEY"))
	call(func() error {
		return phase.Sign(images...)
	})
}

func call(f func() error) {
	if err := f(); err != nil {
		log.WithError(err).Fatal("failed to execute lifecycle phase")
		return
	}
}

func writeValue(dst, val string) error {
	err := os.WriteFile(dst, []byte(val), 0664)
	if err != nil {
		log.WithError(err).Error("failed to write file")
		return err
	}
	return nil
}
