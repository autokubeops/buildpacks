package phase

import (
	"fmt"
	"github.com/paketo-buildpacks/packit/pexec"
	"os"
)

func Analyse(registryImage, previousSha string) error {
	uid := os.Getuid()
	gid := os.Getgid()
	return pexec.NewExecutable(ExecAnalyse).Execute(pexec.Execution{
		Args: []string{
			"-layers=/layers",
			"-group=/layers/group.toml",
			"-analyzed=/layers/analyzed.toml",
			"-cache-dir=/cache",
			fmt.Sprintf("-uid=%d", uid),
			fmt.Sprintf("-gid=%d", gid),
			fmt.Sprintf("%s:%s", registryImage, previousSha),
		},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	})
}
