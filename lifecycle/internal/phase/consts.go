package phase

const (
	ExecDetect  = "/cnb/lifecycle/detector"
	ExecAnalyse = "/cnb/lifecycle/analyzer"
	ExecRestore = "/cnb/lifecycle/restorer"
	ExecBuild   = "/cnb/lifecycle/builder"
	ExecExport  = "/cnb/lifecycle/exporter"
	ExecCreate  = "/cnb/lifecycle/creator"
	ExecSign    = "/usr/local/bin/cosign"
)
