package phase

import (
	"fmt"
	"github.com/paketo-buildpacks/packit/pexec"
	"os"
	"path/filepath"
)

func Create(projectDir, subPath, registryImage, previousSha, runImage string, tags ...string) error {
	uid := os.Getuid()
	gid := os.Getgid()
	return pexec.NewExecutable(ExecCreate).Execute(pexec.Execution{
		Args: append(append([]string{
			fmt.Sprintf("-app=%s", filepath.Join(projectDir, subPath)),
			"-cache-dir=/cache",
			"-layers=/layers",
			fmt.Sprintf("-uid=%d", uid),
			fmt.Sprintf("-gid=%d", gid),
			fmt.Sprintf("-previous-image=%s:%s", registryImage, previousSha),
			fmt.Sprintf("-run-image=%s", runImage),
		}, tags...), fmt.Sprintf("%s:latest", registryImage)),
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	})
}
