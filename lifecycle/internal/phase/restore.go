package phase

import (
	"fmt"
	"github.com/paketo-buildpacks/packit/pexec"
	"os"
)

func Restore() error {
	uid := os.Getuid()
	gid := os.Getgid()
	return pexec.NewExecutable(ExecRestore).Execute(pexec.Execution{
		Args: []string{
			"-group=/layers/group.toml",
			"-layers=/layers",
			"-cache-dir=/cache",
			fmt.Sprintf("-uid=%d", uid),
			fmt.Sprintf("-gid=%d", gid),
		},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	})
}
