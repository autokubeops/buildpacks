package phase

import (
	"fmt"
	"github.com/paketo-buildpacks/packit/pexec"
	"os"
	"strings"
	"time"
)

func Sign(images ...string) error {
	for _, image := range images {
		err := pexec.NewExecutable(ExecSign).Execute(pexec.Execution{
			Args: []string{
				"sign",
				fmt.Sprintf("--key=%s", os.Getenv("COSIGN_PRIVATE_KEY")),
				"-a",
				fmt.Sprintf("ci.commit.sha=%s", os.Getenv("CI_COMMIT_SHA")),
				"-a",
				fmt.Sprintf("ci.commit.ref_slug=%s", os.Getenv("CI_COMMIT_REF_SLUG")),
				"-a",
				fmt.Sprintf("ci.job.started_at=%s", os.Getenv("CI_JOB_STARTED_AT")),
				"-a",
				fmt.Sprintf("ci.job.finished_at=%s", time.Now().Format(time.RFC3339)),
				"-a",
				fmt.Sprintf("ci.job.url=%s", os.Getenv("CI_JOB_URL")),
				"-a",
				fmt.Sprintf("ci.job.image=%s", os.Getenv("CI_JOB_IMAGE")),
				"-a",
				fmt.Sprintf("ci.project.url=%s", os.Getenv("CI_PROJECT_URL")),
				"-a",
				fmt.Sprintf("ci.project.classification=%s", os.Getenv("CI_PROJECT_CLASSIFICATION_LABEL")),
				"-a",
				fmt.Sprintf("ci.pipeline.url=%s", os.Getenv("CI_PIPELINE_URL")),
				"-a",
				fmt.Sprintf("ci.runner.version=%s", os.Getenv("CI_RUNNER_VERSION")),
				strings.TrimPrefix(image, "-tag="),
			},
			Stdout: os.Stdout,
			Stderr: os.Stderr,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
