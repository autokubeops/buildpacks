package phase

import (
	"fmt"
	"github.com/paketo-buildpacks/packit/pexec"
	"os"
	"path/filepath"
)

func Export(projectDir, subPath, runImage string, images ...string) error {
	uid := os.Getuid()
	gid := os.Getgid()
	return pexec.NewExecutable(ExecExport).Execute(pexec.Execution{
		Args: append([]string{
			fmt.Sprintf("-app=%s", filepath.Join(projectDir, subPath)),
			"-layers=/layers",
			"-analyzed=/layers/analyzed.toml",
			"-group=/layers/group.toml",
			"-cache-dir=/cache",
			fmt.Sprintf("-uid=%d", uid),
			fmt.Sprintf("-gid=%d", gid),
			fmt.Sprintf("-run-image=%s", runImage),
		}, images...),
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	})
}
