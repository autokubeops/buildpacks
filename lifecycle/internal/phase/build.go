package phase

import (
	"fmt"
	"github.com/paketo-buildpacks/packit/pexec"
	"os"
	"path/filepath"
)

func Build(projectDir, subPath string) error {
	return pexec.NewExecutable(ExecBuild).Execute(pexec.Execution{
		Args: []string{
			fmt.Sprintf("-app=%s", filepath.Join(projectDir, subPath)),
			"-layers=/layers",
			"-group=/layers/group.toml",
			"-plan=/layers/plan.toml",
		},
		Stdout: os.Stdout,
		Stderr: os.Stderr,
	})
}
