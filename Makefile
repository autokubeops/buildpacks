.PHONY: build
build:
	cd miniserv; make build
	cd npm-ci; make build

.PHONY: package
package:
	cd miniserv; make package
	cd npm-ci; make package