module gitlab.com/autokubeops/buildpacks/miniserv

go 1.17

require github.com/paketo-buildpacks/packit v1.1.0

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/Masterminds/semver/v3 v3.1.1 // indirect
	github.com/VividCortex/ewma v1.1.1 // indirect
	github.com/cheggaaa/pb/v3 v3.0.8 // indirect
	github.com/fatih/color v1.10.0 // indirect
	github.com/gabriel-vasile/mimetype v1.3.1 // indirect
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-runewidth v0.0.12 // indirect
	github.com/pelletier/go-toml v1.9.4 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/ulikunitz/xz v0.5.10 // indirect
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5 // indirect
	golang.org/x/sys v0.0.0-20210603125802-9665404d3644 // indirect
)
