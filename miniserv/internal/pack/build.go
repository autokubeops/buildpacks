package pack

import (
	"fmt"
	"github.com/paketo-buildpacks/packit"
	"github.com/paketo-buildpacks/packit/cargo"
	"github.com/paketo-buildpacks/packit/chronos"
	"github.com/paketo-buildpacks/packit/draft"
	"github.com/paketo-buildpacks/packit/postal"
	"github.com/paketo-buildpacks/packit/scribe"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func Build(entryResolver draft.Planner, clock chronos.Clock, logger scribe.Logger) packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {
		logger.Title("%s %s", context.BuildpackInfo.Name, context.BuildpackInfo.Version)

		// fetch our layer
		srvLayer, err := context.Layers.Get(Miniserv)
		if err != nil {
			return packit.BuildResult{}, err
		}
		srvLayer.Launch = true

		command := filepath.Join(srvLayer.Path, Miniserv)
		service := postal.NewService(cargo.NewTransport())
		entry, _ := entryResolver.Resolve(Miniserv, context.Plan.Entries, nil)
		version, ok := entry.Metadata["version"].(string)
		if !ok {
			version = "default"
		}
		dependency, err := service.Resolve(
			filepath.Join(context.CNBPath, "buildpack.toml"),
			entry.Name,
			version,
			context.Stack,
		)
		if err != nil {
			return packit.BuildResult{}, err
		}

		//buildMetadata := packit.BuildMetadata{BOM: service.GenerateBillOfMaterials(dependency)}
		launchMetadata := packit.LaunchMetadata{
			Processes: []packit.Process{
				{
					Type:    "web",
					Command: command,
					Direct:  true,
				},
			},
		}
		staticPath := os.Getenv("BP_STATIC_DIR")
		if staticPath != "" {
			staticPath = filepath.Join(context.WorkingDir, staticPath)
		}
		envPath := os.Getenv("BP_STATIC_ENV")
		if envPath != "" {
			envPath = filepath.Join(context.WorkingDir, envPath)
		}

		cachedSHA, ok := srvLayer.Metadata[DependencyCacheKey].(string)
		if ok && cachedSHA == getDependencySHA(dependency.SHA256, staticPath, envPath) {
			logger.Process("Reusing cached layer %s", srvLayer.Path)
			logger.Break()

			return packit.BuildResult{
				Plan:   context.Plan,
				Layers: []packit.Layer{srvLayer},
				//Build: buildMetadata,
				Launch: launchMetadata,
			}, nil
		}

		logger.Process("Executing build process")

		srvLayer, err = srvLayer.Reset()
		if err != nil {
			return packit.BuildResult{}, err
		}

		logger.Subprocess("Installing Miniserv %s", dependency.Version)

		duration, err := clock.Measure(func() error {
			return service.Deliver(dependency, context.CNBPath, srvLayer.Path, context.Platform.Path)
		})
		if err != nil {
			return packit.BuildResult{}, err
		}
		logger.Action("Completed in %s", duration.Round(time.Millisecond))
		logger.Break()

		// set launch process
		if staticPath == "" {
			return packit.BuildResult{}, fmt.Errorf("BP_STATIC_DIR is not set")
		}
		srvLayer.LaunchEnv = packit.Environment{
			"SRV_STATIC_DIR": staticPath,
		}
		if envPath != "" {
			srvLayer.LaunchEnv.Default("SRV_DOT_ENV", envPath)
		}

		// fill the types table with our
		// intentions
		srvLayer.Launch = true
		srvLayer.Metadata = map[string]interface{}{
			DependencyCacheKey: getDependencySHA(dependency.SHA256, staticPath, envPath),
			"built_at":         clock.Now().Format(time.RFC3339Nano),
		}

		logger.Process("Assigning launch processes")
		logger.Subprocess("web: %s", command)
		logger.Break()

		return packit.BuildResult{
			Plan:   context.Plan,
			Layers: []packit.Layer{srvLayer},
			//Build: buildMetadata,
			Launch: launchMetadata,
		}, nil
	}
}

func getDependencySHA(str ...string) string {
	return strings.Join(str, "")
}
