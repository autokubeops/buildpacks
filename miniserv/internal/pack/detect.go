package pack

import (
	"fmt"
	"github.com/paketo-buildpacks/packit"
	"os"
	"path/filepath"
)

func Detect() packit.DetectFunc {
	return func(context packit.DetectContext) (packit.DetectResult, error) {
		// look for the package.json
		// todo support the BP_NODE_PROJECT_PATH environment variable
		_, err := os.Stat(filepath.Join(context.WorkingDir, "package.json"))
		if err != nil {
			if os.IsNotExist(err) {
				return packit.DetectResult{}, packit.Fail
			}
			return packit.DetectResult{}, fmt.Errorf("failed to stat package.json: %w", err)
		}

		return packit.DetectResult{
			Plan: packit.BuildPlan{
				Provides: []packit.BuildPlanProvision{
					{Name: Miniserv},
				},
				Requires: []packit.BuildPlanRequirement{
					{
						Name:     Miniserv,
						Metadata: map[string]interface{}{"launch": true},
					},
					{
						Name:     Node,
						Metadata: map[string]interface{}{"build": true},
					},
					{
						Name:     NPM,
						Metadata: map[string]interface{}{"build": true},
					},
					{
						Name:     LayerTarget,
						Metadata: map[string]interface{}{"build": true},
					},
				},
			},
		}, nil
	}
}
