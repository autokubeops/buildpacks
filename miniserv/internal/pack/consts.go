package pack

const (
	Node               = "node"
	LayerTarget        = "npm_build"
	NPM                = "npm"
	Miniserv           = "miniserv"
	DependencyCacheKey = "dependency-sha"
)
