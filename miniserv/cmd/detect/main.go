package main

import (
	"github.com/paketo-buildpacks/packit"
	"gitlab.com/autokubeops/buildpacks/miniserv/internal/pack"
)

func main() {
	packit.Detect(pack.Detect())
}
