package main

import (
	"github.com/paketo-buildpacks/packit"
	"github.com/paketo-buildpacks/packit/chronos"
	"github.com/paketo-buildpacks/packit/draft"
	"github.com/paketo-buildpacks/packit/scribe"
	"gitlab.com/autokubeops/buildpacks/miniserv/internal/pack"
	"os"
)

func main() {
	packit.Build(pack.Build(
		draft.NewPlanner(),
		chronos.DefaultClock,
		scribe.NewLogger(os.Stdout),
	))
}
