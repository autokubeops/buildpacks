package pack

const (
	Node               = "node"
	NPM                = "npm"
	DependencyCacheKey = "dependency-sha"

	LayerCache  = "npm_cache"
	LayerTarget = "npm_build"
)

const (
	PackageLock = "package-lock.json"
	ShrinkWrap  = "npm-shrinkwrap.json"
)

const (
	ConfigScope    = "BP_NPM_SCOPE"
	ConfigRegistry = "BP_NPM_REGISTRY"
	ConfigToken    = "BP_NPM_TOKEN"
)
