package pack

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestConfigure_getOrDefault(t *testing.T) {
	assert.NoError(t, os.Setenv("K", "V"))
	assert.EqualValues(t, "V", getOrDefault("K", ""))
	assert.EqualValues(t, "A", getOrDefault("Z", "A"))
}
