package pack

import (
	"bytes"
	"fmt"
	"github.com/paketo-buildpacks/packit/pexec"
	"github.com/paketo-buildpacks/packit/scribe"
	"os"
	"strings"
)

func Configure(logger scribe.Logger, path string) error {
	exec := pexec.NewExecutable("npm")
	buffer := bytes.NewBuffer(nil)

	scope := getOrDefault(ConfigScope, "registry")
	registry := getOrDefault(ConfigRegistry, "registry.npmjs.org")
	token := getOrDefault(ConfigToken, "")

	args := []string{
		"config",
		"set",
		fmt.Sprintf("%s=https://%s", scope, registry),
	}
	logger.Subprocess("Running 'npm %s'", strings.Join(args, " "))
	err := exec.Execute(pexec.Execution{
		Args: []string{
			"config",
			"set",
			fmt.Sprintf("%s=https://%s", scope, registry),
		},
		Dir:    path,
		Stdout: buffer,
		Stderr: buffer,
	})
	if err != nil {
		logger.Subprocess(buffer.String())
		return err
	}
	// if there's no token, we can bail out early
	if token == "" {
		logger.Subprocess("Skipping credential configuration")
		return nil
	}
	buffer.Reset()
	logger.Subprocess("Running 'npm set //%s/:_authToken [redacted]'", registry)
	err = exec.Execute(pexec.Execution{
		Args: []string{
			"set",
			fmt.Sprintf("//%s/:_authToken", registry),
			token,
		},
		Dir:    path,
		Stdout: buffer,
		Stderr: buffer,
	})
	if err != nil {
		logger.Subprocess(buffer.String())
		return err
	}
	return nil
}

// getOrDefault returns the environment variable 'key'
// and if it is not set or an empty string it returns
// 'value'
func getOrDefault(key, value string) string {
	val := os.Getenv(key)
	if val == "" {
		return value
	}
	return val
}
