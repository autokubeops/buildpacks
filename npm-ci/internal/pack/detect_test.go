package pack

import (
	"github.com/paketo-buildpacks/packit"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDetect(t *testing.T) {
	_, err := Detect()(packit.DetectContext{
		WorkingDir: "./testdata",
	})
	assert.NoError(t, err)

	_, err = Detect()(packit.DetectContext{
		WorkingDir: ".",
	})
	assert.ErrorIs(t, err, packit.Fail)
}

func TestDetect_anyExist(t *testing.T) {
	var cases = []struct {
		files []string
		ok    bool
	}{
		{
			[]string{"./testdata/package-lock.json"},
			true,
		},
		{
			[]string{"./testdata/package-lock.json", "./testdata/npm-shrinkwrap.json"},
			true,
		},
		{
			[]string{},
			false,
		},
	}
	for _, tt := range cases {
		t.Run("", func(t *testing.T) {
			ok, err := anyExist(tt.files...)
			assert.NoError(t, err)
			assert.EqualValues(t, tt.ok, ok)
		})
	}
}
