package pack

import (
	"bytes"
	"fmt"
	"github.com/otiai10/copy"
	"github.com/paketo-buildpacks/packit/pexec"
	"github.com/paketo-buildpacks/packit/scribe"
	"os"
	"path/filepath"
	"strings"
)

type BuildConfiguration struct {
	Logger scribe.Logger
	Path   string
	Build  string
	Output string
}

type Builder struct{}

func NewBuilder() *Builder {
	return new(Builder)
}

func (*Builder) Execute(config BuildConfiguration) error {
	buffer := bytes.NewBuffer(nil)
	// build the project
	args := []string{"run-script", "build"}
	config.Logger.Subprocess("Running 'npm %s'", strings.Join(args, " "))
	err := pexec.NewExecutable(NPM).Execute(pexec.Execution{
		Args:   args,
		Dir:    config.Path,
		Stdout: buffer,
		Stderr: buffer,
		Env: append(
			os.Environ(),
			fmt.Sprintf("NPM_CONFIG_LOGLEVEL=%s", "error"),
		),
	})
	if err != nil {
		config.Logger.Action(buffer.String())
		return err
	}
	// set permissions
	buffer.Reset()
	buildPath := filepath.Join(config.Path, config.Build)
	config.Logger.Subprocess("Updating permissions of '%s'", buildPath)
	err = pexec.NewExecutable("chmod").Execute(pexec.Execution{
		Args:   []string{"-R", "g=u", buildPath},
		Dir:    config.Path,
		Stdout: buffer,
		Stderr: buffer,
	})
	if err != nil {
		config.Logger.Action(buffer.String())
		return err
	}

	// copy built files to a layer
	return copy.Copy(
		buildPath,
		filepath.Join(config.Output, config.Build),
	)
}
