package pack

import (
	"bytes"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/otiai10/copy"
	gobuild "github.com/paketo-buildpacks/go-build"
	"github.com/paketo-buildpacks/packit"
	"github.com/paketo-buildpacks/packit/chronos"
	"github.com/paketo-buildpacks/packit/pexec"
	"github.com/paketo-buildpacks/packit/scribe"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func Build(clock chronos.Clock, logger scribe.Logger) packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {
		logger.Title("%s %s", context.BuildpackInfo.Name, context.BuildpackInfo.Version)
		// get a layer that we can store the cache in
		cacheLayer, err := context.Layers.Get(LayerCache)
		if err != nil {
			return packit.BuildResult{}, err
		}
		cacheLayer.Build = true
		cacheLayer.Cache = true

		buffer := bytes.NewBuffer(nil)
		exec := pexec.NewExecutable(NPM)

		dependencySHA, err := getDependencySHA(context.WorkingDir)
		if err != nil {
			return packit.BuildResult{}, fmt.Errorf("failed to get project signature: %w", err)
		}

		// check if the cached layer signature matches
		// our current one
		cachedSHA, ok := cacheLayer.Metadata[DependencyCacheKey].(string)
		if cachedSHA != "" && ok && cachedSHA == dependencySHA {
			logger.Process("We should reuse cache layer %s", cacheLayer.Path)
			logger.Break()
		}

		// configure npm credentials
		err = Configure(logger, context.WorkingDir)
		if err != nil {
			return packit.BuildResult{}, err
		}

		logger.Process("Executing build process")

		// install dependencies
		args := []string{"ci", "--include=dev", "--unsafe-perm", "--cache", cacheLayer.Path}
		logger.Subprocess("Running 'npm %s'", strings.Join(args, " "))
		duration, err := clock.Measure(func() error {
			return exec.Execute(pexec.Execution{
				Args:   args,
				Dir:    context.WorkingDir,
				Stdout: buffer,
				Stderr: buffer,
				Env: append(
					os.Environ(),
					fmt.Sprintf("NPM_CONFIG_LOGLEVEL=%s", "error"),
				),
			})
		})
		if err != nil {
			logger.Subprocess(buffer.String())
			return packit.BuildResult{}, fmt.Errorf("npm ci failed: %w", err)
		}
		cacheLayer.Metadata = map[string]interface{}{
			DependencyCacheKey: dependencySHA,
			"built_at":         clock.Now().Format(time.RFC3339Nano),
		}

		logger.Action("Completed in %s", duration.Round(time.Millisecond))
		logger.Break()

		// prep a target layer that the webserver will use
		targetLayer, err := context.Layers.Get(LayerTarget)
		if err != nil {
			return packit.BuildResult{}, err
		}

		// do the build
		buildDir := "build"
		if val := os.Getenv("BP_NODE_OUTPUT_DIR"); val != "" {
			buildDir = val
		}
		duration, err = clock.Measure(func() error {
			return NewBuilder().Execute(BuildConfiguration{
				Logger: logger,
				Path:   context.WorkingDir,
				Build:  buildDir,
				Output: targetLayer.Path,
			})
		})
		if err != nil {
			return packit.BuildResult{}, fmt.Errorf("npm build failed: %w", err)
		}
		logger.Action("Completed in %s", duration.Round(time.Millisecond))
		logger.Break()

		targetLayer.Launch = true
		targetLayer.Metadata = map[string]interface{}{
			"built_at": clock.Now().Format(time.RFC3339Nano),
		}

		// nuke the source code
		logger.Subprocess("Deleting source code")
		if err := gobuild.NewSourceDeleter().Clear(context.WorkingDir); err != nil {
			return packit.BuildResult{}, err
		}

		if err := copy.Copy(filepath.Join(targetLayer.Path, buildDir), filepath.Join(context.WorkingDir, buildDir)); err != nil {
			return packit.BuildResult{}, err
		}

		return packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				cacheLayer,
				targetLayer,
			},
		}, nil
	}
}

func getDependencySHA(path string) (string, error) {
	file := filepath.Join(path, PackageLock)
	if ok, err := fileExists(file); ok && err == nil {
		return getSignature(file)
	}
	file = filepath.Join(path, ShrinkWrap)
	if ok, err := fileExists(file); ok && err == nil {
		return getSignature(file)
	} else {
		return "", err
	}
}

func getSignature(path string) (string, error) {
	file, err := os.Open(path)
	if err != nil {
		return "", err
	}
	hash := sha256.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}
	return hex.EncodeToString(hash.Sum(nil)), nil
}
