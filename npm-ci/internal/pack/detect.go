package pack

import (
	"github.com/paketo-buildpacks/packit"
	"os"
	"path/filepath"
)

func Detect() packit.DetectFunc {
	return func(context packit.DetectContext) (packit.DetectResult, error) {
		// look for the package.json
		// todo support the BP_NODE_PROJECT_PATH environment variable
		if ok, err := anyExist(filepath.Join(context.WorkingDir, PackageLock), filepath.Join(context.WorkingDir, ShrinkWrap)); !ok {
			return packit.DetectResult{}, packit.Fail
		} else if err != nil {
			return packit.DetectResult{}, err
		}

		return packit.DetectResult{
			Plan: packit.BuildPlan{
				Provides: []packit.BuildPlanProvision{
					{Name: LayerCache},
					{Name: LayerTarget},
				},
				Requires: []packit.BuildPlanRequirement{
					{
						Name:     LayerCache,
						Metadata: map[string]interface{}{"build": true},
					},
					{
						Name:     Node,
						Metadata: map[string]interface{}{"build": true},
					},
					{
						Name:     NPM,
						Metadata: map[string]interface{}{"build": true},
					},
				},
			},
		}, nil
	}
}

func anyExist(files ...string) (bool, error) {
	for _, f := range files {
		if ok, err := fileExists(f); !ok {
			return false, packit.Fail
		} else if err != nil {
			return false, err
		} else {
			return true, nil
		}
	}
	return false, nil
}

func fileExists(path string) (bool, error) {
	if _, err := os.Stat(path); err == nil {
		return true, nil
	} else if os.IsNotExist(err) {
		return false, nil
	} else {
		return false, err
	}
}
