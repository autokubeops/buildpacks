# AutoKubeOps NPM Compile Cloud Native Buildpack

The NPM Compile CNB makes use of the [`npm`](https://npmjs.com) tooling installed within the [Node Engine CNB](https://github.com/paketo-buildpacks/node-engine) to install application dependencies and build an output.

It is designed for use with compiled NodeJS applications such as React or Vue that are intended to be served statically.

## Integration

The NPM Compile CNB provides `npm_build` as a dependency. Downstream buildpacks can require the `npm_build` dependency by generating a [Build Plan TOML](https://github.com/buildpacks/spec/blob/master/buildpack.md#build-plan-toml) that looks like the following:

```toml
[[requires]]
	name = "npm_build"

	[requires.metadata]
		# Ensures that output files will be available to future buildpacks
		build = true
		# Ensures that output files will be available in running application
		launch = true
```

## Build Configuration

You can set the following environment variables at build either directly (e.g. `pack build my-app --env BP_ENV=value`) or through a [`project.toml` file](https://github.com/buildpacks/spec/blob/main/extensions/project-descriptor.md)

### `BP_NODE_OUTPUT_DIR`

The `BP_NODE_OUTPUT_DIR` allows you to specify the location of the compiled static files.
The default value is `build`

```bash
BP_NODE_OUTPUT_DIR=dist
```

### `BP_KEEP_FILES`

The `BP_KEEP_FILES` variable allows to you to specify a path list of files (including file globs) that you would like to appear in the workspace of the final image. This will allow you to perserve static assests.

```bash
BP_KEEP_FILES=assets/*:public/*
```

### BP_NPM_SCOPE

The `BP_NPM_SCOPE` variables allows you to set the scope of the given registry.
Default value is `registry`

```bash
BP_NPM_SCOPE=@myorg:registry
```

### BP_NPM_REGISTRY

The `BP_NPM_REGISTRY` variable allows you to set the hostname of the default registry.
Default value is `registry.npmjs.org`.

Registries are assumed to be HTTPS.

```shell
BP_NPM_REGISTRY=npm.example.org
```

### BP_NPM_TOKEN

The `BP_NPM_TOKEN` variable allows you to set any credentials needed to access the configured registry.

```shell
BP_NPM_TOKEN=hunter2
BP_NPM_TOKEN=${CI_JOB_TOKEN}
```
