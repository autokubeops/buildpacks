package main

import (
	"github.com/paketo-buildpacks/packit"
	"github.com/paketo-buildpacks/packit/chronos"
	"github.com/paketo-buildpacks/packit/scribe"
	"gitlab.com/autokubeops/buildpacks/npm-ci/internal/pack"
	"os"
)

func main() {
	packit.Run(
		pack.Detect(),
		pack.Build(chronos.DefaultClock, scribe.NewLogger(os.Stdout)),
	)
}
